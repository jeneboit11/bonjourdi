package isima.bonjourdi;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.IOException;
import org.xml.sax.SAXException;
import java.io.File;
import org.w3c.dom.Document;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.HashMap;

public class Configuration {
  private String configFileName;
  private HashMap<Class, Class> map = new HashMap<Class, Class>();
  //private List<Association> associations;

  /*public Configuration(String configFileName) {
    this.configFileName = configFileName;
    associations = new ArrayList<Association>();
    setup();
  }*/

  public void setup() {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    try {
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document= builder.parse(new File(configFileName));
        Element racine = document.getDocumentElement();
        if(racine.getNodeName() == "beans") {
            NodeList racineNodes = racine.getChildNodes();
            int nbracineNodes = racineNodes.getLength();
            for (int i = 0; i<nbracineNodes; i++) {
              Node node = racineNodes.item(i);
              if(node.getNodeType() == Node.ELEMENT_NODE) {
                  Element element = (Element)node;
                  String key = element.getAttribute("id");
                  String value = element.getAttribute("class");
              }
            }
        }
    }
    catch (final ParserConfigurationException e) {
      e.printStackTrace();
    }
    catch (final SAXException e) {
      e.printStackTrace();
    }
    catch (final IOException e) {
      e.printStackTrace();
    }
  }

  public <T> T getInstance(Class<T> c) {
    Field[] fields = c.getFields();
    T o = null;
    try {
      o = c.newInstance();
      for (Field field : fields) {
        Class type = field.getType();
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
          if (annotation.annotationType().equals(Inject.class)) {
            if (map.containsKey(type)) {
              Class injectedClass = map.get(type);
              Constructor constructor = injectedClass.getConstructor();
              field.set(o, constructor.newInstance(new Object[] {}));
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return o;
  }

  public void register(Class a, Class b) {
    map.put(a, b);
  }

  public HashMap<Class, Class> getMap() {
    return map;
  }
}
