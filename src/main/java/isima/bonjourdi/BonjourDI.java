package isima.bonjourdi;

public class BonjourDI {
  private Configuration configuration;

  public BonjourDI() {
    configuration = new Configuration();
  }

  public <T> T getInstance(Class<T> c) {
    return configuration.getInstance(c);
  }

  public void register(Class a, Class b) {
    configuration.register(a, b);
  }

  public Configuration getConfiguration() {
    return configuration;
  }
}
