import isima.bonjourdi.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BonjourDITest {

  private BonjourDI bonjourDI = new BonjourDI();

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {

	}

  @Test
  public final void testRegister() {
    bonjourDI.register(MovieFinder.class, WebMovieFinder.class);
    assertEquals(1, bonjourDI.getConfiguration().getMap().size());

    MovieLister ml = bonjourDI.getInstance(MovieLister.class);
    ml.printMovieList();
    assertNotNull(ml);
  }

  @After
	public void tearDown() throws Exception {

	}

  @AfterClass
	public static void tearDownAfterClass() throws Exception {

	}
}
