import isima.bonjourdi.*;

public class MovieLister {
    @Inject
    public MovieFinder mfinder;

    public void printMovieList() {
        for (String s : mfinder.movieList) {
            System.out.println(s);
        }
    }
}
